#!/usr/bin/env bash

rm -rf icons
mkdir icons
rm -rf screens
mkdir screens


ruby icongen.rb model/icon.png
ruby splashgen.rb model/splash.png universal

mv icons ../main/resources
mv screens ../main/resources

cp appdescriptor.xml ../main/resources

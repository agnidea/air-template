#!/usr/bin/env ruby
# encoding : utf-8

require 'rubygems'
require 'RMagick'
include Magick

# arguments processing
if ARGV.empty?
  puts 'no argument!s given'
  exit
else
  img_name = ARGV[0]
  unless File.exists?(img_name)
    puts "no file named #{img_name}"
    exit
  end
end

device = (ARGV.count > 1 ? ARGV[1] : 'iphone')
img = ImageList.new(img_name)

airicons = [{
      name: 'image29x29',
      size: 29
}, {
      name: 'image40x40',
      size: 40
}, {
      name: 'image50x50',
      size: 50
}, {
      name: 'image57x57',
      size: 57
}, {
      name: 'image60x60',
      size: 60
}, {
      name: 'image72x72',
      size: 72
}, {
      name: 'image76x76',
      size: 76
}, {
      name: 'image80x80',
      size: 80
}, {
      name: 'image100x100',
      size: 100
}, {
      name: 'image114x114',
      size: 114
}, {
      name: 'image120x120',
      size: 120
}, {
      name: 'image144x144',
      size: 144
}, {
      name: 'image512x512',
      size: 512
}, {
      name: 'image1024x1024',
      size: 1024
}]



# delete all icon files
`rm icons/*.png` unless Dir['icons/*.png'].empty?

# resize
airicons.each do |s|
  scaled_img = img.resize_to_fit(s[:size], s[:size])
  filename = 'icons/' + s[:name] +'.png'
  # puts s[:size]
  puts filename
  # log(size, filename)
  scaled_img.write(filename)
end